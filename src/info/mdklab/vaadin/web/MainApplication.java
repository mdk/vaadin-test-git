package info.mdklab.vaadin.web;

import com.vaadin.terminal.Terminal;
import com.vaadin.ui.Window;
import info.mdklab.vaadin.ui.MainWindow;

public class MainApplication extends StdApplication {

  @Override
  public void init() {
    setMainWindow(new MainWindow());
  }


  @Override
  public void terminalError(Terminal.ErrorEvent event) {
    event.getThrowable().printStackTrace();
    getMainWindow().showNotification("Ошибка", event.getThrowable().toString(), Window.Notification.TYPE_ERROR_MESSAGE);
  }

}
