package info.mdklab.vaadin.web;

import com.vaadin.Application;
import com.vaadin.terminal.gwt.server.HttpServletRequestListener;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class StdApplication extends Application implements HttpServletRequestListener {
  protected HttpServletRequest request;
  protected HttpServletResponse response;

  public void onRequestStart(HttpServletRequest request, HttpServletResponse response) {
    this.request = request;
    this.response = response;
  }

  public void onRequestEnd(HttpServletRequest request, HttpServletResponse response) {
//    UserData.getInstance().save(response);
  }

}
