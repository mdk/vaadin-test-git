package info.mdklab.vaadin.model.database.container;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.IndexedContainer;

import java.sql.SQLException;
import java.util.ArrayList;

public abstract class StdData {
  public static final Object PROPERTY_ID = "ID";
  public static final Object PROPERTY_CAPTION = "CAPTION";

  protected Container createIndexedContainer(Object[] ids) {
    ArrayList<String> list = new ArrayList<String>();
    for (Object id : ids) {
      list.add(id.toString());
    }
    return createIndexedContainer(ids, list.toArray(new String[list.size()]));
  }

  protected Container createIndexedContainer(Object[] ids, String[] captions) {
    assert ids.length == captions.length;
    Container container = new IndexedContainer();
    container.addContainerProperty(PROPERTY_ID, ids.length > 0 ? ids[0].getClass() : Object.class, null);
    container.addContainerProperty(PROPERTY_CAPTION, String.class, "");
    for (int i = 0; i < ids.length; i++) {
      Item item = container.addItem(i);
      Property property = item.getItemProperty(PROPERTY_ID);
      property.setValue(ids[i]);
      property = item.getItemProperty(PROPERTY_CAPTION);
      property.setValue(captions[i]);
    }
    return container;
  }

  protected Container createIndexedContainer(int from, int to) {
    ArrayList<Integer> list = new ArrayList<Integer>();
    for (int i = from; i <= to; i++) {
      list.add(i);
    }
    return createIndexedContainer(list.toArray(new Integer[list.size()]));
  }


  public abstract Container getContainer() throws SQLException;
}
