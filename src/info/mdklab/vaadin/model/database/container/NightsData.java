package info.mdklab.vaadin.model.database.container;

import com.vaadin.data.Container;

public class NightsData extends StdData {
  @Override
  public Container getContainer() {
    return createIndexedContainer(1, 29);
  }
}
