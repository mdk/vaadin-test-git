package info.mdklab.vaadin.model.database.container;

import com.vaadin.data.Container;

public class CountryData extends StdData {
  @Override
  public Container getContainer() {
    @SuppressWarnings({"UnnecessaryLocalVariable"})
    Container countryContainer = createIndexedContainer(
        new String[]{"EGYPT", "TURKEY"},
        new String[]{"Египет", "Турция"});
    return countryContainer;
  }
}
