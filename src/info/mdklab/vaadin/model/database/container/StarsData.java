package info.mdklab.vaadin.model.database.container;

import com.vaadin.data.Container;

public class StarsData extends StdData {
  @Override
  public Container getContainer() {
    return createIndexedContainer(
        new String[]{"2", "3", "4", "5", "HV"},
        new String[]{"2*", "3*", "4*", "5*", "HV"});
  }
}
