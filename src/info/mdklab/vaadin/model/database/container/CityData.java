package info.mdklab.vaadin.model.database.container;

import com.vaadin.data.Container;

public class CityData extends StdData {
  @Override
  public Container getContainer() {
    return createIndexedContainer(
        new String[]{
            "MOSCOW", "SANKT-PETERBURG", "CHELIABINSK", "CHERNOVTSY", "DONETSK",
            "EKATERINBURG", "IVANO-FRANKOVSK", "KALININGRAD", "KAZAN", "KHARKOV",
            "KIEV", "KRASNODAR", "KRASNOYARSK", "LUGANSK", "LVIV", "MINERALNYE-VODY",
            "N-NOVGOROD", "NOVOSIBIRSK", "ODESSA", "OMSK", "PERM", "ROSTOV", "SAMARA",
            "TYUMEN", "UFA", "VOLGOGRAD", "ZAPOROJIE"},
        new String[]{
            "Москва", "Санкт-Петербург", "Челябинск", "Черновцы", "Донецк",
            "Екатеринбург", "Ивано-франковск", "Калининград", "Казань", "Харьков",
            "Киев", "Краснодар", "Красноярск", "Луганск", "Львов", "Минеральные воды",
            "Нижний Новгород", "Новосибирск", "Одесса", "Омск", "Пермь", "Ростов", "Самара",
            "Тюмень", "Уфа", "Волгоград", "Запорожье"});
  }
}
