package info.mdklab.vaadin.model.database.container;

import com.vaadin.data.Container;

public class MealData extends StdData {
  @Override
  public Container getContainer() {
    return createIndexedContainer(
        new String[]{"AI", "BB", "FB", "HB", "UAI"},
        new String[]{"ALL INCLUSIVE", "BED & BREAKFAST", "FULL BOARD", "HALF BOARD", "ULTRA AI"});
  }
}
