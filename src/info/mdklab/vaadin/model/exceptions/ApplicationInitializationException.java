package info.mdklab.vaadin.model.exceptions;

import javax.servlet.ServletException;

public class ApplicationInitializationException extends ServletException {

  public ApplicationInitializationException(String message) {
    super(message);
  }

  public ApplicationInitializationException(String s, Throwable ex) {
    super(s, ex);
  }
}
