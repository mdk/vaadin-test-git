package info.mdklab.vaadin.ui;

import com.vaadin.ui.Window;

import java.util.Map;

public class ErrorWindow extends Window {
  private Throwable exception;

  public ErrorWindow(Throwable e) {
    exception = e;
    e.printStackTrace();
    showError();
  }

  @Override
  public void changeVariables(Object source, Map variables) {
    super.changeVariables(source, variables);
    showError();
  }

  private void showError() {
    showNotification("Ошибка", exception.toString(), Notification.TYPE_ERROR_MESSAGE);
  }
}
