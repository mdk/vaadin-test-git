package info.mdklab.vaadin.ui;

import com.vaadin.addon.treetable.TreeTable;
import com.vaadin.ui.*;
import info.mdklab.vaadin.model.database.container.CityData;

public class MainWindow extends Window {
  private static final String TITLE = "";
  private Table table;

  public MainWindow() {
    setSizeFull();
    setCaption(TITLE);

    VerticalLayout mainExpand = new VerticalLayout();
    mainExpand.setSizeFull();
    mainExpand.setMargin(false);
    mainExpand.setSpacing(false);
    setContent(mainExpand);

    Panel scrollLayout = new Panel();
    scrollLayout.setSizeFull();
    scrollLayout.setScrollable(true);
    mainExpand.addComponent(scrollLayout);
    mainExpand.setExpandRatio(scrollLayout, 1);

    Layout results = createResults();
    results.setWidth(100, UNITS_PERCENTAGE);
    scrollLayout.addComponent(results);
  }

  private Layout createResults() {
    Layout results = new VerticalLayout();
    results.setSizeFull();

    table = new TreeTable();
    table.setSizeFull();
    table.setHeight("500px");
    table.setImmediate(true);
    table.setContainerDataSource(new CityData().getContainer());

    table.setColumnCollapsingAllowed(true);
    table.setColumnReorderingAllowed(true);
    table.setSortDisabled(true);
    table.setNullSelectionAllowed(false);
    table.setEditable(false);
    table.setSelectable(true);

    results.addComponent(table);

    return results;
  }

}