@ECHO OFF

@ECHO ON
hg pull
@ECHO OFF
if not errorlevel 0 (
  echo Error %errorlevel%.
  goto end
)
@ECHO ON
hg update
@ECHO OFF
if not errorlevel 0 (
  echo Error %errorlevel%.
  goto end
)
@ECHO ON
hg push %*
@ECHO OFF
if not errorlevel 0 (
  echo Error %errorlevel%.
  goto end
)

:end
goto :EOF
